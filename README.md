* Open Science Days 2022 — Guix Tutorial

This repository contains notes and material for the [Guix
tutorial](https://osd-uga-2022.sciencesconf.org/resource/page/id/5) at
[Open Science Days](https://osd-uga-2022.sciencesconf.org/), 13–15
December 2022, Grenoble, France.
