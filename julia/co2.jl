# guix shell -m julia/manifest.scm  -- julia -L julia/co2.jl -i

using DataFrames, CSV
using PyCall
pygui(:tk)
using PyPlot
using LinearAlgebra
using Zygote

# Get data
df = CSV.read("monthly_in_situ_co2_mlo.csv",
              comment="\"", header=1:3, DataFrame)

# From documentation of Mauna Loa Observatory, Hawaii dataset
# https://scrippsco2.ucsd.edu/data/atmospheric_co2/mlo.html
date  = 4
value = 9

# Rescale to mitigate numerical error
# for example dates are 1958.2027 so power may be too large
scale = 100.0


function filtered(vec, val)
    index = Vector{Int64}()
    for i in 1:length(vec)
        if vec[i] != val
            push!(index, i)
        end
    end
    return index
end

index = filtered(df[:, value], -99.99)
x = df[index, date] / scale
y = df[index, value]


function solve(A, b)
    # Penrose pseudo-inverse (QR would be better!)
    L,U = lu(A' * A)
    sol = U \ (L \ (A' * b))
    return sol
end

function poly(x, y, order=1)
    a = ones(length(x))
    xk = copy(x)
    for k in 1:order
        a = hcat(xk, a)
        xk .*= x
    end

    coefs = solve(a, y)

    function f(x)
        # poor man polymorphism
        # (maybe better with multidispatch?)
        to_copy = true
        if isa(x, Number)
            b = 1.0
            to_copy = false
        elseif isa(x, Vector)
            b = ones(length(x))
        else
            b = ones(size(x))
        end

        if to_copy
            xk = copy(x)
        else
            xk = x
        end

        val = coefs[order + 1] * b
        for k in 1:order
            val += coefs[order + 1 - k] * xk
            if to_copy
                xk .*= x
            else
                xk *= x
            end
        end
        return val
    end
    return f
end

flin = poly(x, y, 1)
fqua = poly(x, y, 2)

fcub = poly(x, y, 3)
ften = poly(x, y, 10)


# plot(x,y)
# plot(x, fqua(x))

#
#
#

function newton(target, f; init=19.58)
    old = 0.0
    new = init
    while abs(old - new) > 1e-3
        old = new
        new = old - (f(old) - target) / f'(old)
    end
    return new
end

year = x -> convert(Int, floor(x * scale))
when = (target, f) -> year(newton(target, f))

# when(600, fqua) # = 2078
# when(800, fqua) # = 2122
