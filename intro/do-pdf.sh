#!/usr/bin/env bash

guix time-machine -C channels.scm                \
     -- shell --container -m manifest.scm        \
     -- rubber --pdf intro-tuto-Guix-UGA2022.tex
