\documentclass[aspectratio=169,ignorenonframetext,presentation]{beamer}

%
% Header
%

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{fancyvrb}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, positioning, arrows.meta}

\newcommand{\tikzmark}[2]{%
  \tikz[remember picture,baseline=(#1.base)]%
  {\node[inner sep=0pt] (#1) {#2 \quad};}}

\newcommand{\tikzbrace}[3]{%
  \begin{tikzpicture}[remember picture,overlay]
    \draw[decorate,decoration={brace}] (#1.north east) -- node[right]
    { % XXXX: hfill
      #3}
    (#1.north east |- #2.south east);
  \end{tikzpicture}}


\usetheme{Madrid}
% \useoutertheme[subsection=true]{smoothbars}
\makeatletter
\usecolortheme{whale}
\usecolortheme{orchid}

\setbeamerfont{block title}{size={}}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{itemize items}[triangle]
\makeatletter
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=.20\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
      \usebeamerfont{author in head/foot}Open Science Days@UGA
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.65\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
      \usebeamerfont{title in head/foot}\insertshorttitle
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

\title[Toward reproducible research using Guix]%
{Reproducible deployment for scientific software using GNU Guix}
\author{Ludovic Courtès, Konrad Hinsen, Simon Tournier}
\date{December, 14th, 2022}
\institute{%
  \texttt{ludovic.courtes@inria.fr}\\
  \texttt{konrad.hinsen@cnrs.fr}\\
  \texttt{simon.tournier@inserm.fr}}
\hypersetup{
  pdfauthor={Courtès, Hinsen, Tournier},
  pdftitle={Reproducible deployment for scientific software using GNU Guix},
  pdfkeywords={GNU Guix, reproducible research, open science, package manager, virutal machine},
  pdfsubject={GNU Guix},
  pdfcreator={with love},
  pdflang={English}}

\newenvironment{answer}{%
  \VerbatimEnvironment
  \begin{exampleblock}{}
    \begin{Verbatim}}%
    {\end{Verbatim}
  \end{exampleblock}}


%
% Document
%
\begin{document}
\begin{frame}[fragile]{Warm-up: First things first}

  \begin{answer}
      $ guix pull --commit=b94724e8b2102be0fe9d19e9dfe44d6f7101bd4b
  \end{answer}

  \begin{enumerate}
  \item Install the packages \texttt{git-minimal}, \texttt{nss-certs} and \texttt{which}

    \begin{answer}
      $ guix install git-minimal nss-certs which
    \end{answer}

  \item Clone the repository
    \href{https://gitlab.inria.fr/guix-hpc/open-science-days-tutorial}%
    {https://gitlab.inria.fr/guix-hpc/open-science-days-tutorial}

    and go inside

    \begin{answer}
      $ git clone https://gitlab.inria.fr/guix-hpc/open-science-days-tutorial
      $ cd open-science-days-tutorial
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Warm-up: Yet another package manager}
  \begin{enumerate}
  \item Search with the terms \texttt{high-performance}, \texttt{dynamic},
    \texttt{language}
    \begin{answer}
      $ guix search high-performance dynamic language
    \end{answer}
  \item Display the information corresponding to the Julia package
    \begin{answer}
      $ guix show julia
    \end{answer}
  \item Install the Julia package and start the Julia REPL (\texttt{julia},
    quit \emph{Control-D})
    \begin{answer}
      $ guix install julia
      $ julia
      julia> exit()
    \end{answer}
  \item Install other Julia packages: PyPlot and DataFrames
    \begin{answer}
      $ guix install julia-pyplot julia-dataframes
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Warm-up: Yet another package manager? 1/3}
  \begin{enumerate}
  \item Try \texttt{guix package -{}-list-generations}
    \begin{answer}
      $ guix package --list-generations
Generation 1	Dec 14 2022 13:08:38
  julia	1.6.7	out	/gnu/store/...-julia-1.6.7

Generation 2	Dec 14 2022 13:09:11	(current)
 + julia-dataframes	1.2.2  out  /gnu/store/...-julia-dataframes-1.2.2
 + julia-pyplot    	2.10.0 out  /gnu/store/...-julia-pyplot-2.10.0
    \end{answer}
  \item Remove the package \texttt{julia-pyplot}
    \begin{answer}
      $ guix remove julia-pyplot
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Warm-up: Yet another package manager? 2/3}
  \begin{enumerate}
    \setcounter{enumi}{2}
  \item Try \texttt{guix package -{}-roll-back} and list again the generations
    \begin{answer}
      $ guix package --roll-back
switched from generation 3 to 2
      $ guix package --list-generations
Generation 1	Dec 14 2022 13:08:38
  julia	1.6.7	out	/gnu/store/...-julia-1.6.7

Generation 2	Dec 14 2022 13:09:11	(current)
 + julia-dataframes	1.2.2 	out /gnu/store/...-julia-dataframes-1.2.2
 + julia-pyplot    	2.10.0	out /gnu/store/...-julia-pyplot-2.10.0

Generation 3	Dec 14 2022 13:11:30
 - julia-pyplot	2.10.0	out /gnu/store/...-julia-pyplot-2.10.0
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Warm-up: Yet another package manager? 3/3}
  \begin{enumerate}
    \setcounter{enumi}{3}
  \item Remove PyPlot and install the Julia packages CSV and Zygote with
    only one command-line
    \begin{answer}
      $ guix package --remove julia-pyplot --install julia-csv julia-zygote
    \end{answer}
  \item Try \texttt{guix package -{}-list-installed}
    \begin{answer}
      $ guix package --list-installed
julia           	1.6.7 	out /gnu/store/...-julia-1.6.7
julia-pyplot    	2.10.0	out /gnu/store/...-julia-pyplot-2.10.0
julia-dataframes	1.2.2 	out /gnu/store/...-julia-dataframes-1.2.2
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} \hfill 1/5}
  \begin{enumerate}
  \item Create a shell with the Python packages Python, Pandas, Numpy and
    Matplotlib

    Start \texttt{python3} and try \texttt{import numpy}

    Exit the created shell with \texttt{exit} or \emph{Control-D}
    \begin{answer}
      $ guix shell python python-pandas python-numpy python-matplotlib
      $ python3
      >>> quit()
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} \hfill 2/5}
  \begin{enumerate}
    \setcounter{enumi}{1}
  \item Create a manifest file containing these Python packages
    \begin{answer}
      $ guix shell python python-pandas python-numpy python-matplotlib \
             --export-manifest
;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

(specifications->manifest
  (list "python"
        "python-pandas"
        "python-numpy"
        "python-matplotlib"))

      $ guix shell python python-pandas python-numpy python-matplotlib \
             --export-manifest > manifest.scm
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} \hfill 3/5}
  \begin{enumerate}
    \setcounter{enumi}{2}
  \item Start a new shell using this manifest file

    Run the script \texttt{python/co2.py}

    Exit the created shell.
    \begin{answer}
      $ guix shell --manifest=manifest.scm
      $ python3 python/co2.py
      $ exit
    \end{answer}
  \item Create a new shell and run the script with one command-line
    \begin{answer}
      $ guix shell --manifest=manifest.scm -- python3 python/co2.py
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} \hfill 4/5}
  \begin{enumerate}
    \setcounter{enumi}{4}
  \item Create a new shell using the previous manifest file and start the
    IPython REPL instead

    Try \texttt{\%run python/co2.py} then \texttt{x.shape}, exit and exit.
    \begin{answer}
      $ guix shell -m manifest.scm python-ipython
      $ ipython
      In [1]: %run python/co2.py
      In [2]: x.shape
      Out[2]: (773,)
      In [3]: quit()
      $ exit
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} \hfill 5/5}
  \begin{enumerate}
    \setcounter{enumi}{5}
  \item Using the previous manifest, start a new shell using the IPython REPL

    but unsetting all the existing environment variables

    Try \texttt{which ipython}.  Compare with \texttt{echo \$GUIX\_ENVIRONMENT}.
    Exit.
    \begin{answer}
      $ guix shell -m manifest.scm python-ipython --pure
      $ ls
ls: command not found
      $ which ipython
which: command not found
      $ exit
      $ guix shell -m manifest.scm python-ipython --pure which
      $ which ipython
/gnu/store/3s2vfgb95x8jk3m4mac69fbmmpg5d12p-profile/bin/ipython
      $ echo $GUIX_ENVIRONMENT
/gnu/store/3s2vfgb95x8jk3m4mac69fbmmpg5d12p-profile
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} again \hfill 1/3}
  \begin{enumerate}
  \item Run the script \texttt{python/co2.py} inside a container.

    Try \texttt{which ipython}.  Try to list other directories.  Exit.

    \begin{answer}
      $ guix shell -m manifest.scm python-ipython --container
      $ python3 python/co2.py
      $ cd $HOME/tmp
sh: cd: /home/simon/tmp: No such file or directory
      $ exit
      $ if [ -d $HOME/tmp ]; then echo ok; else echo ko; fi
ok
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} again \hfill 2/3}
  \begin{enumerate}
    \setcounter{enumi}{1}
  \item Create a new shell using the manifest file and
    adding Emacs
    \begin{answer}
      $ guix shell -m manifest.scm python-ipython emacs-minimal
    \end{answer}
  \item Idem but containerized.  Start Emacs.  Exit.
    \begin{answer}
      $ guix shell -m manifest.scm emacs-minimal --container
      $ emacs
 Please set the environment variable TERM; see 'tset'.
      $ exit
      $ guix shell -m manifest.scm emacs-minimal -C --preserve=TERM
      $ emacs # exit with Control-x Control-c
      $ exit
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{\texttt{guix shell} again \hfill 3/3}
  \begin{enumerate}
    \setcounter{enumi}{3}
  \item Re-try considering the option \texttt{-{}-network/-N}.

    Try to preserve the environment variable \texttt{TERM} and
    \texttt{DISPLAY}

    Start Emacs in this container.  Exit.

    \begin{answer}
      $ guix shell -m manifest.scm emacs-minimal -C -N -E TERM -E DISPLAY
      $ emacs
      $ exit
    \end{answer}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Example: \texttt{article/the-article.pdf} \hfill 1/2}
  \begin{enumerate}
  \item Try to reproduce the figure
    \begin{answer}
      $ guix time-machine --commit=b94724e8b2102be0fe9d19e9dfe44d6f7101bd4b \
             -- shell --pure -m python/manifest.scm                         \
             -- python3 python/co2.py
    \end{answer}
  \end{enumerate}
  \vfill
  Bonus: try \texttt{guix lint -c archive python-scikit-learn}

  and browse
  \href{https://archive.softwareheritage.org/save/list/}{https://archive.softwareheritage.org/save/list/}
\end{frame}

\begin{frame}[fragile]{Example: \texttt{article/the-article.pdf} \hfill 2/2}
  \begin{enumerate}
    \setcounter{enumi}{1}
  \item Try to get the same value (see \texttt{python/co2-bis.py})
    \begin{answer}
      $ guix time-machine --channels=python/channels.scm        \
             -- shell --pure -m python/manifest.scm python-dual \
             -- python3 python/co2-bis.py
SWH: found revision 3c51f294b61044fb8e99480ca7ad9ef8c55044e0
with directory at 'https://archive.softwareheritage.org/api/1/directory/06d4371d13f6348fe856e701e1f7951b5e9ddcfe/'
[...]
fatal: could not read Username for 'https://gitlab.com': No such device or address
Failed to do a shallow fetch; retrying a full fetch...
SWH: found revision f2158a0d919013c4547756a920919d901ff21210
with directory at 'https://archive.softwareheritage.org/api/1/directory/f2c7181b4efb13c8eb37f81e763c1a3f9820c477/'
[...]
Year: 2122
\end{answer}
\small{
The channel (describing the Guix recipe of the packages \texttt{python-dual})
is unreachable, as well as the source code of this package.  It is a double
(recursive) fallback to Software Heritage.}
\end{enumerate}
\end{frame}
\end{document}
