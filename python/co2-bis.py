
from co2 import scale, x, y, solve

import numpy as np
from numbers import Number
from math import floor

from dual.dual import Dual

#####
#####

def poly_with_dual(x, y, order=1):
    n = len(x)
    a = np.ones((n, 1))
    try:
        xx = np.array([dn.value for dn in x])
        yy = np.array([dn.value for dn in y])
    except:
        xx = x
    xk = xx
    for k in range(1, order+1):
        a = np.concatenate((xk.reshape(n, 1), a), axis=1)
        xk = np.multiply(xx, xk)

    coefs = solve(a, yy)

    def f(x):
        if isinstance(x, Number):
            b = 1.0
        elif isinstance(x, np.ndarray):
            if isinstance(x[0], Dual):
                b = np.array([Dual.constant("c", 1.0)
                              for _ in x])
            else:
                b = np.ones(x.shape)
        elif isinstance(x, Dual):
            b = Dual.constant("ao", 1.0)
        val = b * coefs[-1]
        xk  = x
        for k in range(1, order+1):
            val += xk * coefs[-1-k]
            xk = np.multiply(x, xk)
        return val
    return f


x = np.array([ Dual.variable("x", xi) for xi in x])
y = np.array([ Dual.variable("y", yi) for yi in y])

fqua = poly_with_dual(x, y, 2)

def newton(target, f, init=Dual.variable("x", 19.58)):
    old = Dual.variable("0", 0.0)
    new = init
    while abs(old.value - new.value) > 1e-3:
        old = new
        fold = f(old)
        new = old - (fold.value - target) / fold.deriv
    return new.value

year = lambda x: int(floor(x * scale))
when = lambda target, f: year(newton(target, f))

print("Year: {} ".format(when(800, fqua)))
