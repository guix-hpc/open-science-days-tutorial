import pandas as pd
import numpy as np
from numbers import Number
from math import floor
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

df = pd.read_csv("monthly_in_situ_co2_mlo.csv",
                 comment="\"", header=[0,1,2])

# From documentation of Mauna Loa Observatory, Hawaii dataset
# https://scrippsco2.ucsd.edu/data/atmospheric_co2/mlo.html
date  = 4 - 1 # 4th column but Python is 0-indexed
value = 9 - 1

# Rescale to mitigate numerical error
# for example dates are 1958.2027 so power may be too large
scale = 100.0

def reformat(df):
    def filtered(vec, val):
        return [i for i,v in enumerate(vec) if v != val]

    def convert(series):
        return series.to_numpy()

    index = filtered(df.iloc[:, value], -99.99)
    x = convert(df.iloc[index, date] / scale)
    y = convert(df.iloc[index, value])
    return (x, y)

x, y = reformat(df)

def solve(A, b):
    sol, _, _, _ = np.linalg.lstsq(A, b)
    return sol

def poly(x, y, order=1):
    n = len(x)
    a = np.ones((n, 1))
    xk = x.copy()
    for k in range(1, order+1):
        a = np.concatenate((xk.reshape(n, 1), a), axis=1)
        np.multiply(x, xk, out=xk)

    coefs = solve(a, y)

    def f(x):
        if isinstance(x, Number):
            b = 1.0
        elif isinstance(x, np.ndarray):
            b = np.ones(x.shape)
            xk = x.copy()

        val = coefs[-1] * b
        for k in range(1, order+1):
            val += coefs[-1-k] * xk
            np.multiply(x, xk, out=xk)
        return val
    return f

flin = poly(x, y, 1)
fqua = poly(x, y, 2)

fcub = poly(x, y, 3)
ften = poly(x, y, 10)


plt.plot(x,y, 'b-')
plt.plot(x,fqua(x), 'r-')
plt.show()
