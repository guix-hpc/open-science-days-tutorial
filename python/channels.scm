(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
         "b94724e8b2102be0fe9d19e9dfe44d6f7101bd4b"))
      (channel
        (name 'extra)
        (url "https://gitlab.com/zimoun/channel4tuto")
        (branch "main")
        (commit
         "3c51f294b61044fb8e99480ca7ad9ef8c55044e0")))
